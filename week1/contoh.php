<?php
$a = "hello";
$b = "world";

echo $a . $b; //menggabungkan
echo "<br>";
echo $a, $b; //mencetak satu persatu

$anak = ['Arif', 'Maman', 'Manda'];
print_r($anak);

$angka1 = 5;
$angka2 = 5.4;

var_dump($angka1);
var_dump($angka2);
var_dump($anak);

/*
 ini adalah blok
 komentar
 */
